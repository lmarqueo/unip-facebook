package br.com.unip.service;

import org.springframework.stereotype.Service;

import br.com.unip.domain.FacebookPage;

@Service
public class FacebookPageService {

	public FacebookPage findByHubToken(String hubToken) {
    	
		if(hubToken.equals("bf198663-f26c-43f4-ad55-9d6def90235f")) {
			
			return FacebookPage.builder()
							   .pageId(103732378175070L)
							   .pageName("Unip-Verde")
					   		   .hubToken("bf198663-f26c-43f4-ad55-9d6def90235f")
					   		   .pageAccessToken("EAAFPamXeplABAFDV5JniPqSXco5RseW5dYYwf1828ckS3fu4E7l0rF8190JNZCOrotF6nbd6Mi7a8TRZCGsMwNxXuKRBKussfHPNBGHnRn4N4iUg1ws9cjdevpC8XXWhSuNCoGzMwoxKUnMH1PVNA3VmcXzpwahZCycIGZCeL8dR3fkmr4jM")
					   		   .build();
		}
		
		return FacebookPage.builder()
						   .build();
    }
	
}
