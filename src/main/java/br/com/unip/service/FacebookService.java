package br.com.unip.service;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unip.cache.FacebookCacheRepository;
import br.com.unip.cache.FacebookUserCache;
import br.com.unip.domain.FacebookInteraction;
import br.com.unip.domain.FacebookMessage;
import br.com.unip.domain.FacebookPage;
import br.com.unip.domain.FacebookUser;
import br.com.unip.domain.enuns.MessageType;
import br.com.unip.dto.MessageDTO;
import br.com.unip.feign.FacebookFeign;
import br.com.unip.feign.WatsonFeign;
import br.com.unip.service.dto.FacebookMessageRequest;
import br.com.unip.watson.request.ConversationRequest;
import br.com.unip.watson.request.ConversationResponse;

@Service
public class FacebookService {

    private static final Logger LOG = LoggerFactory.getLogger(FacebookService.class);
    
    @Autowired
    private FacebookFeign facebookFeign;

    @Autowired
    private WatsonFeign watsonFeign;
    
    @Autowired
    private FacebookPageService facebookPageService;
    
    @Autowired
    private FacebookCacheRepository facebookCacheRepository;
    
    public void conversation(FacebookInteraction facebookInteraction) {
    	
        ConversationRequest conversationRequest = this.prepareConversationRequest(facebookInteraction);
        LOG.info("Request: " + conversationRequest);
        
        ConversationResponse conversationResponse = this.watsonFeign.callNlp(conversationRequest);
        LOG.info("Response: " + conversationResponse);
        
        this.callFacebook(conversationResponse);
    }

    public void callFacebook(ConversationResponse conversationResponse) {

        FacebookMessageRequest facebookMessageRequest = this.prepareFacebookRequest(conversationResponse);
        
        FacebookPage facebookPage = this.facebookPageService.findByHubToken("bf198663-f26c-43f4-ad55-9d6def90235f");

        this.facebookFeign.callFacebook(facebookPage.getPageAccessToken(), facebookMessageRequest);

    }

    private ConversationRequest prepareConversationRequest(FacebookInteraction facebookInteraction) {
    	
    	MessageDTO message = MessageDTO.builder()
    							 .user(this.saveUser(facebookInteraction).getUser())
    							 .content(facebookInteraction.getMessage().getText())
    							 .date(LocalDateTime.now())
    							 .build();
    	
        return new ConversationRequest(message, null);
    }

    private FacebookMessageRequest prepareFacebookRequest(ConversationResponse conversationResponse) {
    	
        return FacebookMessageRequest.builder()
                                     .message(new FacebookMessage(conversationResponse.getMessage().getContent()))
                                     .recipient(new FacebookUser(this.facebookCacheRepository.findById(conversationResponse.getMessage().getUser().getId())
                                    		 												 .get()
                                    		 												 .getUser()))
                                     .messaging_type(MessageType.RESPONSE)
                                     .build();
    }

    private FacebookUserCache saveUser(FacebookInteraction facebookInteraction) {
    	
    	return this.facebookCacheRepository.save(FacebookUserCache.builder()
										   .id(facebookInteraction.getSender().getId())
										   .user(facebookInteraction.getSender())
										   .build());
    }
    
}
