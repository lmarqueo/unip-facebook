package br.com.unip.service.dto;

import java.io.Serializable;

import br.com.unip.domain.FacebookMessage;
import br.com.unip.domain.FacebookUser;
import br.com.unip.domain.enuns.MessageType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class FacebookMessageRequest implements Serializable {
	private static final long serialVersionUID = 2591621956297230963L;

	private MessageType messaging_type;

    private FacebookMessage message;

    private FacebookUser recipient; 

}
