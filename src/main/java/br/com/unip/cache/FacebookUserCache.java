package br.com.unip.cache;

import java.io.Serializable;

import javax.persistence.Id;

import org.springframework.data.redis.core.RedisHash;

import br.com.unip.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@RedisHash("FacebookUser")
public class FacebookUserCache implements Serializable {
	private static final long serialVersionUID = -3088997103264370441L;
	
	@Id
	private Long id;

	private UserDTO user;
	
}
