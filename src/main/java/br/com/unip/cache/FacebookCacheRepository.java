package br.com.unip.cache;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacebookCacheRepository extends JpaRepository<FacebookUserCache, Long> {

}
