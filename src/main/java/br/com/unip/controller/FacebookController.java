package br.com.unip.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.unip.controller.dto.FacebookRequest;
import br.com.unip.domain.FacebookEntry;
import br.com.unip.domain.FacebookInteraction;
import br.com.unip.domain.FacebookPage;
import br.com.unip.service.FacebookPageService;
import br.com.unip.service.FacebookService;

@RestController
@RequestMapping("/facebook")
public class FacebookController {

    private static final Logger LOG = LoggerFactory.getLogger(FacebookController.class);
    
    @Autowired
    private FacebookService facebookService;
    
    @Autowired
    private FacebookPageService facebookPageService;

    @GetMapping("/webhook")
    public ResponseEntity<Object> webhook(@RequestParam("hub.mode") String hubMode, 
                          @RequestParam("hub.verify_token") String hubToken, 
                          @RequestParam("hub.challenge") String hubChallenge) {

        FacebookPage facebookPage = this.facebookPageService.findByHubToken(hubToken);

        LOG.info("Webhook - Hubmode: " + hubMode + " verifyToken: " + hubToken + " Challenge: " + hubChallenge);                   

        if("subscribe".equals(hubMode) && facebookPage.getHubToken().equals(hubToken)) {
        	
            LOG.info("Token is valid");
            
            return ResponseEntity.ok(hubChallenge);
        }
        LOG.info("Token is not valid");
        
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }

    @PostMapping("/webhook")
    public ResponseEntity<Void> conversation(@RequestBody FacebookRequest facebookRequest) {

        if("page".equals(facebookRequest.getObject())) {
        	
            for(FacebookEntry facebookEntry: facebookRequest.getEntry()) {
            	
                if(!CollectionUtils.isEmpty(facebookEntry.getMessaging())) {
                	
                    for(FacebookInteraction message : facebookEntry.getMessaging()) {
                    	
                        try {
                        	
                            this.facebookService.conversation(message);
                            
                            return ResponseEntity.status(HttpStatus.OK).build();
                            
                        } catch (Exception e) {
                        	
                            LOG.error("Error: ", e);
                            
                            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
                        }
                    }
                }
            }
        }
        
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}
