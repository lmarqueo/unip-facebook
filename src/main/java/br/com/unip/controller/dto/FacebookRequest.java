package br.com.unip.controller.dto;

import java.io.Serializable;
import java.util.List;

import br.com.unip.domain.FacebookEntry;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
public class FacebookRequest implements Serializable {
	private static final long serialVersionUID = 7374705516997736446L;

	private String object;

    private List<FacebookEntry> entry;

}
