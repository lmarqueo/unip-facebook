package br.com.unip.controller.dto;

import java.io.Serializable;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
public class FacebookResponse implements Serializable {
	private static final long serialVersionUID = 5230909912769262900L;

	private Map<String, Object> recipient;

    private Map<String, Object> message;

}
