package br.com.unip.domain;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
public class FacebookEntry implements Serializable {
	private static final long serialVersionUID = 1611198564648583047L;

	private String id;

    private Long time;

    private List<FacebookInteraction> messaging;

}
