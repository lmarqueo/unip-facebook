package br.com.unip.domain;

import java.io.Serializable;

import br.com.unip.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter 
public class FacebookInteraction implements Serializable {
	private static final long serialVersionUID = 3872316653017490991L;

	private UserDTO sender;

    private UserDTO recipient;

    private FacebookMessage message;
    
}
