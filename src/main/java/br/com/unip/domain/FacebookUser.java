package br.com.unip.domain;

import java.io.Serializable;

import br.com.unip.dto.UserDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class FacebookUser implements Serializable {
	private static final long serialVersionUID = -3088997103264370441L;
	
	private Long id;

	public FacebookUser(UserDTO user) {
		this.id = user.getId();
	}

}
