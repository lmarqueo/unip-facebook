package br.com.unip.domain.enuns;

import lombok.Getter;

@Getter
public enum MessageType {

    RESPONSE("response"), MESSAGE_TAG("message_tag");

    private String value;

    MessageType(String value){
        this.value = value;
    }

}
