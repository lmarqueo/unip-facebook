package br.com.unip.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class FacebookPage implements Serializable {
	private static final long serialVersionUID = 2516417111785996407L;

	@Id
	private Long pageId;

    private String pageName;

    private String hubToken;

    private String pageAccessToken;

}
