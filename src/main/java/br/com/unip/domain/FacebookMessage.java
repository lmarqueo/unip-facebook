package br.com.unip.domain;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter  
public class FacebookMessage implements Serializable {
	private static final long serialVersionUID = 7253007517281431358L;
	
	private String text;

}
