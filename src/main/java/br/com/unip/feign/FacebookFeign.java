package br.com.unip.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.unip.dto.UserDTO;
import br.com.unip.service.dto.FacebookMessageRequest;

@FeignClient(name = "facebook", url = "https://graph.facebook.com")
public interface FacebookFeign {

    @GetMapping("/{psid}?access_token")
    public UserDTO findUser(@PathVariable("psid") Long psid, @RequestParam("access_token") String accessToken);

    @PostMapping("/v6.0/me/messages?access_token")
    public UserDTO callFacebook(@RequestParam("access_token") String accessToken, @RequestBody FacebookMessageRequest facebookMessageRequest);

}
