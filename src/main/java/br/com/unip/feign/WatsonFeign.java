package br.com.unip.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.unip.watson.request.ConversationRequest;
import br.com.unip.watson.request.ConversationResponse;

@FeignClient(url = "${unip.watson.url}", name = "watson-nlp")
public interface WatsonFeign {

    @PostMapping("/conversation")
    public ConversationResponse callNlp(@RequestBody ConversationRequest conversationRequest);
    
}
